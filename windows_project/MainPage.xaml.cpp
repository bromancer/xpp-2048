﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"
#include "direction.hpp"
#include "tile_interface.hpp"
#include "tile.h"
#include "board.h"

using namespace windows_project;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI;
using namespace Windows::UI::Input;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

MainPage::MainPage()
{
	InitializeComponent();

	this->m_board = xpp::BoardInterface::Create();

	this->m_start_x = 0;
	this->m_start_y = 0;
}

void
MainPage::DrawBoard()
{
	std::unordered_set<std::shared_ptr<xpp::TileInterface>> tiles = this->m_board->GetTiles();

	for ( auto it = std::begin( tiles ); it != std::end( tiles ); )
	{
		std::shared_ptr<xpp::TileInterface> tile = (*it);

		Border ^ b = (Border^) this->FindName( "Cell_" + tile->ToRow() + tile->ToColumn() );
		TextBlock ^ tb = (TextBlock^) this->FindName( "Field_" + tile->ToRow() + tile->ToColumn() );

		if ( tile->GetValue() == 0 )
		{
			tb->Text = "";
		}
		else
		{
			tb->Text = tile->GetValue().ToString();
		}

		Windows::UI::Xaml::Style ^ style = (Windows::UI::Xaml::Style ^) this->Resources->Lookup( "Tile" + tile->GetValue().ToString() + "Style" );

		b->Style = style;

		++it;
	}
}


void
MainPage::root_Loaded( Object^ sender, RoutedEventArgs^ e )
{
	this->DrawBoard();
}


void
MainPage::OnPointerEntered( Object^ sender, PointerRoutedEventArgs^ e )
{
	PointerPoint ^ p = e->GetCurrentPoint( this );

	this->m_start_x = p->Position.X;
	this->m_start_y = p->Position.Y;
}


void
MainPage::OnPointerExited( Object^ sender, PointerRoutedEventArgs^ e )
{
	PointerPoint ^ p = e->GetCurrentPoint( this );
	xpp::Direction direction = xpp::Direction::NONE;

	float delta_x = p->Position.X - this->m_start_x;
	float delta_y = p->Position.Y - this->m_start_y;

	if ( abs( delta_x ) > (3 * abs( delta_y )) && abs( delta_x ) > 30 )
	{
		direction = delta_x > 0 ? xpp::Direction::RIGHT : xpp::Direction::LEFT;
	}
	else if ( abs( delta_y ) > (3 * abs( delta_x )) && abs( delta_y ) > 30 )
	{
		direction = delta_y > 0 ? xpp::Direction::DOWN : xpp::Direction::UP;
	}

	if ( direction != xpp::Direction::NONE )
	{
		this->m_board->Move( direction );
		this->DrawBoard();
	}

}
