﻿//
// CustomControl.h
// Declaration of the CustomControl class.
//

#pragma once

namespace windows_project
{
	public ref class CustomControl sealed : public Windows::UI::Xaml::Controls::Control
	{
	public:
		CustomControl();
	};
}
