﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//

#pragma once

#include "MainPage.g.h"
#include "board_interface.hpp"

namespace windows_project
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public ref class MainPage sealed
	{
	private:
		std::shared_ptr<xpp::BoardInterface> m_board;
		float m_start_x;
		float m_start_y;
	public:
		MainPage();
		void DrawBoard();

	private:
		void root_Loaded( Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e );
		void OnPointerEntered( Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e );
		void OnPointerExited( Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e );
	};
}
