//
//  ViewController.m
//  Game2048
//
//  Created by Michiel Hegemans on 29/03/16.
//  Copyright © 2016 Michiel Hegemans. All rights reserved.
//

#import "ViewController.h"
#import "XPPBoardInterface.h"
#import "XPPDirection.h"
#import "XPPTileInterface.h"

@import UIKit;

@interface ViewController ()

@end

@implementation ViewController {
    XPPBoardInterface *_Board;
    NSSet<XPPTileInterface *> * _Tiles;
}



-(IBAction)swipedLeft:(UISwipeGestureRecognizer *)recognizer {
    [_Board move:XPPDirectionLeft];
    [self drawBoard];
}
-(IBAction)swipedRight:(UISwipeGestureRecognizer *)recognizer {
    [_Board move:XPPDirectionRight];
    [self drawBoard];
}
-(IBAction)swipedUp:(UISwipeGestureRecognizer *)recognizer {
    [_Board move:XPPDirectionUp];
    [self drawBoard];
}
-(IBAction)swipedDown:(UISwipeGestureRecognizer *)recognizer {
    [_Board move:XPPDirectionDown];
    [self drawBoard];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedLeft:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(swipedRight:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc]  initWithTarget:self action:@selector(swipedUp:)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUp];
    
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedDown:)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDown];
    
    _Board = [XPPBoardInterface create];
    
    [self drawBoard];
}

- (void)drawBoard {
    NSSet<XPPTileInterface *> * tiles = [_Board getTiles];
    
    for (XPPTileInterface* tile in tiles) {
        UILabel * label = [self getLabel:tile.toRow withCol:tile.toColumn];
        if (tile.getValue == 0) {
            label.text = @"";
        } else {
            label.text = [NSString stringWithFormat:@"%i", tile.getValue];
        }
    }
}

- (UILabel*)getLabel:(int32_t)row withCol:(int32_t)col {
    if (row == 0 && col == 0) {
        return Cell_00;
    } else if (row == 0 && col == 1) {
        return Cell_01;
    } else if (row == 0 && col == 2) {
        return Cell_02;
    } else if (row == 0 && col == 3) {
        return Cell_03;
    } else if (row == 1 && col == 0) {
        return Cell_10;
    } else if (row == 1 && col == 1) {
        return Cell_11;
    } else if (row == 1 && col == 2) {
        return Cell_12;
    } else if (row == 1 && col == 3) {
        return Cell_13;
    } else if (row == 2 && col == 0) {
        return Cell_20;
    } else if (row == 2 && col == 1) {
        return Cell_21;
    } else if (row == 2 && col == 2) {
        return Cell_22;
    } else if (row == 2 && col == 3) {
        return Cell_23;
    } else if (row == 3 && col == 0) {
        return Cell_30;
    } else if (row == 3 && col == 1) {
        return Cell_31;
    } else if (row == 3 && col == 2) {
        return Cell_32;
    } else {
        return Cell_33;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
