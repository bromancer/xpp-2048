//
//  AppDelegate.h
//  Game2048
//
//  Created by Michiel Hegemans on 29/03/16.
//  Copyright © 2016 Michiel Hegemans. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

