//
//  ViewController.h
//  Game2048
//
//  Created by Michiel Hegemans on 29/03/16.
//  Copyright © 2016 Michiel Hegemans. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    IBOutlet UILabel * Cell_00;
    IBOutlet UILabel * Cell_01;
    IBOutlet UILabel * Cell_02;
    IBOutlet UILabel * Cell_03;
    
    IBOutlet UILabel * Cell_10;
    IBOutlet UILabel * Cell_11;
    IBOutlet UILabel * Cell_12;
    IBOutlet UILabel * Cell_13;
    
    IBOutlet UILabel * Cell_20;
    IBOutlet UILabel * Cell_21;
    IBOutlet UILabel * Cell_22;
    IBOutlet UILabel * Cell_23;
    
    IBOutlet UILabel * Cell_30;
    IBOutlet UILabel * Cell_31;
    IBOutlet UILabel * Cell_32;
    IBOutlet UILabel * Cell_33;

}

- (void)drawBoard;
- (UILabel*)getLabel:(int32_t)row withCol:(int32_t)col;

@end

