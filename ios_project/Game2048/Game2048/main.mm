//
//  main.m
//  Game2048
//
//  Created by Michiel Hegemans on 29/03/16.
//  Copyright © 2016 Michiel Hegemans. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
