package net.bromancer.game2048;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import net.bromancer.game2048.shared.BoardInterface;
import net.bromancer.game2048.shared.Direction;
import net.bromancer.game2048.shared.TileInterface;

import java.util.HashSet;

public class GameBoard extends AppCompatActivity {

    static {
        System.loadLibrary("gnustl_shared");
        System.loadLibrary("xpp");
    }

    private BoardInterface board;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_board);

        this.board = BoardInterface.create();

        move(Direction.DOWN);

        drawBoard();
    }

    /**
     * Called after determining which direction the user
     * made a movement with his or her finger.
     *
     * @param direction Direction of the movement.
     */
    private void move(Direction direction) {
        // Moves tiles on the board.
        this.board.move(direction);
        // Redraw board afterwards.
    }

    /**
     * Gets the new board state and draws all tiles.
     */
    private void drawBoard() {
        HashSet<TileInterface> tiles = board.getTiles();

        for (TileInterface tile : tiles) {
            int value = tile.getValue();

            // @TODO: Draw tiles on grid.
        }
    }
}
