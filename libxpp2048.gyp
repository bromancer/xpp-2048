{
	"targets": [
		{
			"target_name": "libxpp2048_objc",
			"type": "static_library",
			"dependencies": [
				"./deps/djinni/support-lib/support_lib.gyp:djinni_objc"
			],
			"direct_dependent_settings": {

			},
			"sources": [
				"<!@(python deps/djinni/example/glob.py generated-src/objc '*.cpp' '*.mm' '*.m')",
				"<!@(python deps/djinni/example/glob.py generated-src/cpp '*.cpp')",
				"<!@(python deps/djinni/example/glob.py shared-src/cpp '*.cpp')"
			],
			"include_dirs": [
				"generated-src/objc",
				"generated-src/cpp",
				"shared-src/cpp"
			]
		}
	]
}