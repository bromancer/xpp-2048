#include "board.h"
#include "tile.h"
#include "direction.hpp"
#include <vector>
#include <stdlib.h>

namespace xpp
{

bool
Board::MoveLeft()
{
	bool has_changed = false;
	std::shared_ptr<Tile> tile;
	std::shared_ptr<Tile> target_tile;

	for ( int row = 0; row < BOARD_SIZE; row++ )
	{
		std::vector<std::shared_ptr<Tile>> current_row;
		std::shared_ptr<Tile> result_row[BOARD_SIZE];

		for ( int col = 0; col < BOARD_SIZE; col++ )
		{
			tile = this->m_cells[row][col];

			if ( tile != nullptr && tile->GetValue() != 0 )
			{
				current_row.push_back( tile );
			}
		}

		for ( int target = 0; target < BOARD_SIZE; target++ )
		{
			if ( current_row.size() > 0 )
			{
				target_tile = current_row.at( 0 );
				current_row.erase( current_row.begin() );
			}
			else
			{
				target_tile = this->AddTile();
			}

			if ( current_row.size() > 0 &&
				 (current_row.at( 0 )->GetValue() == target_tile->GetValue()) )
			{
				std::shared_ptr<Tile> tile1 = target_tile;
				target_tile = this->AddTile( target_tile->GetValue() * 2 );
				tile1->MergeInto( target_tile );

				std::shared_ptr<Tile> tile2 = current_row.at( 0 );
				current_row.erase( current_row.begin() );

				tile2->MergeInto( target_tile );
			}

			result_row[target] = target_tile;

			this->m_won = this->m_won ? this->m_won : (target_tile->GetValue() == 2048);

			has_changed = has_changed ? has_changed : (target_tile->GetValue() != this->m_cells[row][target]->GetValue());
		}

		for ( int col = 0; col < BOARD_SIZE; col++ )
		{
			this->m_cells[row][col] = result_row[col];
		}
	}

	return has_changed;
}

bool
Board::MoveUp()
{
	bool has_changed = false;
	std::shared_ptr<Tile> tile;
	std::shared_ptr<Tile> target_tile;

	for ( int col = 0; col < BOARD_SIZE; col++ )
	{
		std::vector<std::shared_ptr<Tile>> current_col;
		std::shared_ptr<Tile> result_col[BOARD_SIZE];

		for ( int row = 0; row < BOARD_SIZE; row++ )
		{
			tile = this->m_cells[row][col];

			if ( tile != nullptr && tile->GetValue() != 0 )
			{
				current_col.push_back( tile );
			}
		}

		for ( int target = 0; target < BOARD_SIZE; target++ )
		{
			if ( current_col.size() > 0 )
			{
				target_tile = current_col.at( 0 );
				current_col.erase( current_col.begin() );
			}
			else
			{
				target_tile = this->AddTile();
			}

			if ( current_col.size() > 0 &&
				 (current_col.at( 0 )->GetValue() == target_tile->GetValue()) )
			{
				std::shared_ptr<Tile> tile1 = target_tile;
				target_tile = this->AddTile( target_tile->GetValue() * 2 );
				tile1->MergeInto( target_tile );

				std::shared_ptr<Tile> tile2 = current_col.at( 0 );
				current_col.erase( current_col.begin() );

				tile2->MergeInto( target_tile );
			}

			result_col[target] = target_tile;

			this->m_won = this->m_won ? this->m_won : (target_tile->GetValue() == 2048);

			has_changed = has_changed ? has_changed : (target_tile->GetValue() != this->m_cells[target][col]->GetValue());
		}

		// Direction does not matter for update.
		for ( int row = 0; row < BOARD_SIZE; row++ )
		{
			this->m_cells[row][col] = result_col[row];
		}
	}

	return has_changed;
}

bool
Board::MoveDown()
{
	bool has_changed = false;
	std::shared_ptr<Tile> tile;
	std::shared_ptr<Tile> target_tile;

	for ( int col = BOARD_SIZE - 1; col >= 0; --col )
	{
		std::vector<std::shared_ptr<Tile>> current_col;
		std::shared_ptr<Tile> result_col[BOARD_SIZE];

		for ( int row = BOARD_SIZE - 1; row >= 0; --row )
		{
			tile = this->m_cells[row][col];

			if ( tile != nullptr && tile->GetValue() != 0 )
			{
				current_col.push_back( tile );
			}
		}

		for ( int target = BOARD_SIZE - 1; target >= 0; --target )
		{
			if ( current_col.size() > 0 )
			{
				target_tile = current_col.at(0);
				current_col.erase( current_col.begin() );
			}
			else
			{
				target_tile = this->AddTile();
			}

			if ( current_col.size() > 0 &&
				 (current_col.at( 0 )->GetValue() == target_tile->GetValue()) )
			{
				std::shared_ptr<Tile> tile1 = target_tile;
				target_tile = this->AddTile( target_tile->GetValue() * 2 );
				tile1->MergeInto( target_tile );

				std::shared_ptr<Tile> tile2 = current_col.at( 0 );
				current_col.erase( current_col.begin() );

				tile2->MergeInto( target_tile );
			}

			result_col[target] = target_tile;

			this->m_won = this->m_won ? this->m_won : (target_tile->GetValue() == 2048);

			has_changed = has_changed ? has_changed : (target_tile->GetValue() != this->m_cells[target][col]->GetValue());
		}

		// Direction does not matter for update.
		for ( int row = 0; row < BOARD_SIZE; row++ )
		{
			this->m_cells[row][col] = result_col[row];
		}
	}

	return has_changed;
}

bool
Board::MoveRight()
{
	bool has_changed = false;
	std::shared_ptr<Tile> tile;
	std::shared_ptr<Tile> target_tile;

	for ( int row = BOARD_SIZE - 1; row >= 0; --row )
	{
		std::vector<std::shared_ptr<Tile>> current_row;
		std::shared_ptr<Tile> result_row[BOARD_SIZE];

		for ( int col = BOARD_SIZE - 1; col >= 0; --col )
		{
			tile = this->m_cells[row][col];

			if ( tile != nullptr && tile->GetValue() != 0 )
			{
				current_row.push_back( tile );
			}
		}

		for ( int target = BOARD_SIZE - 1; target >= 0; --target )
		{
			if ( current_row.size() > 0 )
			{
				target_tile = current_row.at( 0 );
				current_row.erase( current_row.begin() );
			}
			else
			{
				target_tile = this->AddTile();
			}

			if ( current_row.size() > 0 &&
				 (current_row.at( 0 )->GetValue() == target_tile->GetValue()) )
			{
				std::shared_ptr<Tile> tile1 = target_tile;
				target_tile = this->AddTile( target_tile->GetValue() * 2 );
				tile1->MergeInto( target_tile );

				std::shared_ptr<Tile> tile2 = current_row.at( 0 );
				current_row.erase( current_row.begin() );

				tile2->MergeInto( target_tile );
			}

			result_row[target] = target_tile;

			this->m_won = this->m_won ? this->m_won : (target_tile->GetValue() == 2048);

			has_changed = has_changed ? has_changed : (target_tile->GetValue() != this->m_cells[row][target]->GetValue());
		}

		// Direction does not matter for update.
		for ( int col = 0; col < BOARD_SIZE; col++ )
		{
			this->m_cells[row][col] = result_row[col];
		}
	}

	return has_changed;
}

std::shared_ptr<Tile>
Board::AddTile()
{
	return this->AddTile( 0 );
}

std::shared_ptr<Tile>
Board::AddTile( int32_t value )
{
	const std::shared_ptr<Tile> tile = std::make_shared<Tile>( value );

	return tile;
}

void
Board::SetPositions()
{
	std::shared_ptr<Tile> tile;
	this->m_tiles.clear();

	for ( int row = 0; row < BOARD_SIZE; row++ )
	{
		for ( int col = 0; col < BOARD_SIZE; col++ )
		{
			tile = this->m_cells[row][col];

			// Update positions.
			tile->SetOldRow( tile->GetRow() );
			tile->SetOldColumn( tile->GetColumn() );
			tile->SetRow( row );
			tile->SetColumn( col );

			// Do not mark tile for deletion.
			tile->MarkForDeletion( false );

			this->m_tiles.insert( tile );
		}
	}
}

void
Board::AddRandomTile()
{
	std::vector<std::shared_ptr<Tile>> empty_cells;
	std::shared_ptr<Tile> tile;
	int index;
	std::shared_ptr<Tile> cell;
	int32_t new_value;

	for ( int row = 0; row < BOARD_SIZE; row++ )
	{
		for ( int col = 0; col < BOARD_SIZE; col++ )
		{
			tile = this->m_cells[row][col];

			if ( tile->GetValue() == 0 )
			{
				tile->SetRow( row );
				tile->SetColumn( col );
				empty_cells.insert( empty_cells.begin(), tile );
			}
		}
	}

	index = rand() % empty_cells.size();
	cell = empty_cells.at( index );
	new_value = (rand() % 100) < FOUR_PORBABILITY ? 4 : 2;

	this->m_cells[cell->GetRow()][cell->GetColumn()] = this->AddTile( new_value );
}

/// <summary>
/// </summary>
Board::Board()
	: m_tiles( {} )
{
	// Fill matrix with tiles.
	for ( int row = 0; row < BOARD_SIZE; row++ )
	{
		for ( int col = 0; col < BOARD_SIZE; col++ )
		{
			this->m_cells[row][col] = this->AddTile();
		}
	}

	this->AddRandomTile();
	this->SetPositions();
	this->m_won = false;
}

/// <summary>
/// </summary>
Board::~Board()
{
}

/// <summary>
/// </summary>
std::shared_ptr<BoardInterface>
BoardInterface::Create()
{
	return std::make_shared<Board>();
}

std::unordered_set<std::shared_ptr<TileInterface>>
Board::GetTiles()
{
	return this->m_tiles;
}

void
Board::Move( Direction direction )
{
	bool has_changed = false;

	switch ( direction )
	{
	case Direction::UP:
		has_changed = MoveUp();
		break;
	case Direction::DOWN:
		has_changed = MoveDown();
		break;
	case Direction::LEFT:
		has_changed = MoveLeft();
		break;
	case Direction::RIGHT:
		has_changed = MoveRight();
		break;
	default:
		break;
	}

	if ( has_changed )
	{
		this->AddRandomTile();
	}

	this->SetPositions();
}

bool
Board::HasWon()
{
	return this->m_won;
}

bool
Board::HasLost()
{
	bool can_move = false;
	std::shared_ptr<Tile> tile;
	int delta_x[] = { -1,0,1,0 };
	int delta_y[] = { 0,-1,0,1 };
	int new_row;
	int new_column;

	for ( int row = 0; row < BOARD_SIZE; row++ )
	{
		for ( int col = 0; col < BOARD_SIZE; col++ )
		{
			tile = this->m_cells[row][col];

			can_move = can_move ? can_move : (this->m_cells[row][col]->GetValue() == 0);

			/*if ( this->m_cells[i][j]->GetValue() == 0 )
			{
				return false;
			}*/

			for ( int dir = 0; dir < 4; dir++ )
			{
				new_row = row + delta_x[dir];
				new_column = col + delta_y[dir];

				if ( new_row < 0 || new_row >= BOARD_SIZE || new_column < 0 || new_column >= BOARD_SIZE )
				{
					continue;
				}

				can_move = can_move ? can_move : (this->m_cells[row][col]->GetValue() == this->m_cells[new_row][new_column]->GetValue());

				/*if ( this->m_cells[i][j]->GetValue() == this->m_cells[new_row][new_column]->GetValue() )
				{
					return false;
				}*/
			}
		}
	}

	return !can_move;
}

}