#pragma once

#include "tile_interface.hpp"
#include <memory>

namespace xpp
{

class Tile : public TileInterface
{
private:
	static int id;
	int m_id;
	bool m_marked;
	int m_old_row;
	int m_old_column;
	int m_row;
	int m_column;
	int32_t m_value;
	std::shared_ptr<Tile> m_merged;

public:
	Tile();
	Tile( int32_t value );
	Tile( int32_t value, int row, int column );
	~Tile();
	int32_t GetValue();
	int32_t GetId();

	void MoveTo( int row, int column );
	bool IsNew();
	bool HasMoved();
	int FromRow();
	int32_t ToRow();
	int FromColumn();
	int32_t ToColumn();

	void SetOldRow( int row );
	void SetOldColumn( int column );
	void SetRow( int row );
	void SetColumn( int column );
	void MarkForDeletion( bool mark );
	void MergeInto( std::shared_ptr<TileInterface> target );

	int GetOldRow();
	int GetOldColumn();
	int GetRow();
	int GetColumn();
	bool IsMarkedForDeletion();
	std::shared_ptr<TileInterface> GetMerged();
};

}