#pragma once

#include "board_interface.hpp"
#include "tile_interface.hpp"
#include "tile.h"
#include "direction.h"

#define BOARD_SIZE 4        // Board is 4x4.
#define FOUR_PORBABILITY 10 // 10 percent chance.

namespace xpp
{

class Board : public xpp::BoardInterface
{
private:
	std::unordered_set<std::shared_ptr<TileInterface>> m_tiles;
	std::shared_ptr<Tile> m_cells[BOARD_SIZE][BOARD_SIZE];
	bool m_won;

	bool MoveLeft();
	bool MoveUp();
	bool MoveDown();
	bool MoveRight();

	std::shared_ptr<Tile> AddTile();
	std::shared_ptr<Tile> AddTile(int32_t value);
	void SetPositions();
	void AddRandomTile();

public:
	Board();
	~Board();

	std::unordered_set<std::shared_ptr<TileInterface>> GetTiles();
	void Move( Direction direction );
	bool HasWon();
	bool HasLost();
};

}