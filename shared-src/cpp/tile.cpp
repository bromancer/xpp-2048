#include "tile.h"

namespace xpp
{

int 
Tile::id = 0;

Tile::Tile() : Tile( 0 )
{
}

Tile::Tile( int32_t value ) : Tile( value, -1, -1 )
{
}

Tile::Tile( int32_t value, int row, int column )
{
	this->m_value = value;
	this->m_row = row;
	this->m_column = column;

	this->m_old_row = -1;
	this->m_old_column = -1;

	this->m_merged = nullptr;
	this->m_id = id++;
	this->m_marked = false;
}


Tile::~Tile()
{

}

int32_t
Tile::GetValue()
{
	return this->m_value;
}

int32_t
Tile::GetId()
{
	return this->m_id;
}

void
Tile::MoveTo( int row, int column )
{
	this->m_old_row = this->m_row;
	this->m_old_column = this->m_column;

	this->m_row = row;
	this->m_column = column;
}

bool
Tile::IsNew()
{
	return this->m_old_row == -1 && (this->m_merged == nullptr);
}

bool
Tile::HasMoved()
{
	return (this->FromRow() != -1) &&
		((this->FromRow() != this->m_row) || (this->FromColumn() != this->ToColumn()) ||
		(this->m_merged != nullptr));
}

int
Tile::FromRow()
{
	if ( this->GetMerged() == nullptr )
	{
		return this->m_old_row;
	}
	else
	{
		return this->m_row;
	}
}

int32_t
Tile::ToRow()
{
	if ( this->m_merged == nullptr )
	{
		return this->m_row;
	}
	else
	{
		return this->m_merged->m_row;
	}
}

int
Tile::FromColumn()
{
	if ( this->m_merged == nullptr )
	{
		return this->m_old_column;
	}
	else
	{
		return this->m_column;
	}
}

int32_t
Tile::ToColumn()
{
	if ( this->m_merged == nullptr )
	{
		return this->m_column;
	}
	else
	{
		return this->m_merged->m_column;
	}
}

void
Tile::SetOldRow( int row )
{
	this->m_old_row = row;
}

void
Tile::SetOldColumn( int column )
{
	this->m_old_column = column;
}

void
Tile::SetRow( int row )
{
	this->m_row = row;
}

void
Tile::SetColumn( int column )
{
	this->m_column = column;
}

void
Tile::MarkForDeletion( bool mark )
{
	this->m_marked = mark;
}

void
Tile::MergeInto( std::shared_ptr<TileInterface> target )
{
	this->m_merged = std::dynamic_pointer_cast<Tile>(target);
}

int
Tile::GetOldRow()
{
	return this->m_old_row;
}

int
Tile::GetOldColumn()
{
	return this->m_old_column;
}

int
Tile::GetRow()
{
	return this->m_row;
}

int
Tile::GetColumn()
{
	return this->m_column;
}

bool
Tile::IsMarkedForDeletion()
{
	return this->m_marked;
}

std::shared_ptr<TileInterface>
Tile::GetMerged()
{
	return this->m_merged;
}

}