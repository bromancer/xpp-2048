./build_ios/libxpp2048.xcodeproj: libxpp2048.gyp ./deps/djinni/support-lib/support_lib.gyp djinni/shared.djinni
	sh ./run_djinni.sh
	deps/gyp/gyp --depth=. -f xcode -DOS=ios --generator-output ./build_ios -Ideps/djinni/common.gypi ./libxpp2048.gyp

ios: ./build_ios/libxpp2048.xcodeproj
	xcodebuild clean build -workspace ios_project/Game2048.xcworkspace \
		-scheme Game2048 \
		-configuration 'Debug' \
		-sdk iphonesimulator \
		-destination 'platform=iOS Simulator,name=iPhone 6'
