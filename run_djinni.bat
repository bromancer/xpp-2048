@echo off

set base_dir=%cd%
set cpp_out=%base_dir%\generated-src\cpp
set jni_out=%base_dir%\generated-src\jni
set objc_out=%base_dir%\generated-src\objc
set java_out=%base_dir%\generated-src\java\net\bromancer\game2048\shared
set java_package=net.bromancer.game2048.shared
set namespace=xpp
set objc_prefix=XPP
set djinni_file=%base_dir%\djinni\shared.djinni

%base_dir%\deps\djinni\src\target\start.bat ^
    --java-out %java_out% ^
    --java-package %java_package% ^
    --ident-java-field mFooBar ^
    ^
    --cpp-out %cpp_out% ^
    --cpp-namespace %namespace% ^
    --ident-cpp-method FooBar ^
    --ident-cpp-field m_foobar ^
    --ident-cpp-file foo_bar ^
    ^
    --jni-out %jni_out% ^
    --ident-jni-class NativeFooBar ^
    --ident-jni-file NativeFooBar ^
    ^
    --objc-out %objc_out% ^
    --objc-type-prefix %objc_prefix% ^
    ^
    --objcpp-out %objc_out% ^
    ^
    --idl %djinni_file%